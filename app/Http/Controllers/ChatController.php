<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SettingRoom;
use App\Room;

class ChatController extends Controller
{

    public function chat_room_list(){
      $rooms = Room::all();
      view()->share('rooms', $rooms);
      return view('room-list');
    }

    public function create_room(Request $request){
        $this->validate($request, [
        'name' => 'required',
        ]);

     // $room_id = 'room-'.strtolower(str_random(4));
      $room = new Room();
      $room->name = $request->name;
      $room->save();
      return redirect('/')->with('success', 'Room Created Succesfully');
    }

    public function add_user_to_room(Request $request){
        $username = $request->username;
        $pass = $request->password;
        $room_id = $request->room_id;


        $userCheck = SettingRoom::where('username', $username)
                    ->where('pass', $pass)
                    ->where('room_id', $room_id)
                    ->first();

        if($userCheck){
          $users = SettingRoom::where('room_id', $room_id)->get()->pluck('username','nick', 'pass');

          return view('chat_2', [
            'nick' => $userCheck->nick, 
            'users' => $users, 
            'fio' => 'group_call', 
            'microphoneonly' => 1
            ]);
        }else{

          $count_users = count(SettingRoom::all());
          $setting_room = new SettingRoom();
          $setting_room->room_id = $room_id;
          $setting_room->username = $username;
          $setting_room->nick = 'user'.($count_users + 1);
          $setting_room->pass = $pass;
          $setting_room->save();

          // Update Room Collection
          $room = Room::where('id', $room_id)->first();
          $room->count_users = $room->count_users + 1;
          $room->save();

        return redirect('/')->with('success', 'User Added to Room Succesfully');

        }            

    }

    public function enter_room_by_id($room_id){
      $users = SettingRoom::where('room_id', $room_id)->get()->pluck('username','nick', 'pass');

      return view('chat_2', [
        'nick' => 'Prodip', 
        'users' => $users, 
        'fio' => 'group', 
        'microphoneonly' => 1
        ]);

    }

    public function login(){
      $users = SettingRoom::where('room_id', 1)->get()->pluck('nick', 'pass');
      //dump($users);
      return view('login',['users' => $users]);
    }

    public function chat(Request $request){
      $this->validate($request, [
        'nick' => 'required',
        'pass' => 'required',
        'fio' => 'required',
        ]);

      if(SettingRoom::where('nick', $request->input('nick'))->where('pass', $request->input('pass'))->where('room_id', 1)->count() != 1)
      {
        return redirect()->back();
      }

      // $users = ['igor', 'serzh'];
      $users = SettingRoom::where('room_id', 1)->get()->pluck('nick');


      return view('chat_2', ['nick' => $request->input('nick'), 'users' => $users, 'fio' => $request->input('fio'), 'microphoneonly' => $request->input('camera')]);
    }

    public function test(){
      return view('test');
    }
}
