<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClassRoomController extends Controller
{
    public function my_room(){
     return View('class-room.my-room');
    }

    public function scholars_room(){
     return View('class-room.scholars-room');
    }

    public function multiconnection_group_call(){
        return View('class-room.multiconnection-group-call');
    }

    public function scholars_multiconnection_group_call(){
        return View('class-room.group-call-file-sharing');
    }

    public function group_conference(){
        return View('class-room.group-canvas-design');
    }
}
