<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = 'rooms';

    public function setting()
    {
        return $this->hasMany('App\SettingRoom', 'room_id', 'id');
    }
}
