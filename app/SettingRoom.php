<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingRoom extends Model
{
    protected $table = 'setting_rooms';
}
