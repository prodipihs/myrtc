<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>chat</title>

         <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" crossorigin="anonymous">


        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style type="text/css">
          html { height: 100%; }
          body { height: 100%; margin: 0; background: #333; text-align: center; }
          .video { height: 215px; margin-top: 5%; background: #000; }
          .localVideo { width: 150px; position: absolute; right: 1.1em; bottom: 1em; border: 1px solid #333; background: #000; z-index: 100;}
          #callButton {
          position: absolute;
          /* display: none; */
          left: 70%;
          font-size: 2em;
          top: 5%;
          border-radius: 1em;
        }

        }
      h3 {
          font-size: 24px;
          color: #fff !important;
      }

      /* SNACKBAR */
      /* The snackbar - position it at the bottom and in the middle of the screen */
      #snackbar {
          visibility: hidden; /* Hidden by default. Visible on click */
          min-width: 250px; /* Set a default minimum width */
        /*  margin-left: -125px; /* Divide value of min-width by 2 */
          background-color: rgba(158,31,99,0.9); /* background color */
          color: #fff; /* White text color */
          text-align: center; /* Centered text */
          border-radius: 2px; /* Rounded borders */
          padding: 16px; /* Padding */
          position: fixed; /* Sit on top of the screen */
          z-index: 1; /* Add a z-index if needed */
          left: 0; /* Right the snackbar */
          top: 90px; /* 90px from the top */
      }

      /* Show the snackbar when clicking on a button (class added with JavaScript) */
      #snackbar.show {
          visibility: visible; /* Show the snackbar */
      }
          
        </style>
    </head>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
              <h1 style="color:white;"> Current User: {{$nick}}</h1>
                  <!--Snackbar -->
                  <div id="snackbar"></div>
                  <!-- Snackbar -->
              <br>
              <video id="localVideo-{{$nick}}"  class="localVideo" autoplay muted></video>

              <button id="callButton" onclick="createOffer()">✆</button>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <h3 style="color:#fff;">User 1</h3>
                  <video id="remoteVideo-user1" class='video' autoplay></video>
                </div>
                <div class="col-md-4 col-xs-12">
                  <h3 style="color:#fff;">User 2</h3>
                  <video id="remoteVideo-user2" class='video' autoplay></video>
                </div>
                <div class="col-md-4 col-xs-12">
                   <h3 style="color:#fff;">User 3</h3>
                   <video id="remoteVideo-user3" class='video' autoplay></video>
                </div>
              </div>
              
              
             

            </div>
        </div>

        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        First PC, first offer: <span id="11"></span><br/>
        First PC, second offer: <span id="12"></span><br/>
        Second PC, first offer: <span id="21"></span><br/>
        Second PC, second offer: <span id="22"></span><br/> -->
    </body>
     <!-- <script src="{{ asset('js/adapter.js') }}"></script> -->
     <script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
    <script>

      var PeerConnection = window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
      var IceCandidate = window.mozRTCIceCandidate || window.RTCIceCandidate;
      var SessionDescription = window.mozRTCSessionDescription || window.RTCSessionDescription;
      navigator.getUserMedia = navigator.getUserMedia || navigator.mozGetUserMedia || navigator.webkitGetUserMedia;

      var pc_user1;
      var pc_user2;
      var pc_user3;
      var streamConstraints;


        var servers = {

          // iceServers: [{
          //   urls: ["stun:stun.l.google.com:19302"]
          // }]

        iceServers: [{
          // Scholars-home-live turn server
            urls: 'turn:52.88.28.2:3478',
            username: 'turnuser',
            credential: 'turn.pass'

          // Google stun server
          //  urls: 'stun:stun.l.google.com:19302',
        }]
        };


      // WebSocket
   //var conn = new WebSocket('wss://webrtc.local:443/wss2/');
 //  var conn = new WebSocket("wss://webrtc-test.ru/wss2/");
  //  var conn = new WebSocket('wss:/scholars-home.org/chatsocket');

    conn = new WebSocket('ws:/localhost:8080');
      

      // conn = new WebSocket('wss://localhost:2424');
      // console.log('websocekt', conn);

      //  conn = new WebSocket("wss://scholars-home.org/chatsocket");

    

    window.addEventListener('load', function () {




    function hasGetUserMedia() {
      return !!(navigator.getUserMedia || navigator.mediaDevices.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
    }

        console.log('media_status: ', hasGetUserMedia);


    if (hasGetUserMedia) {
      console.log('into the media');
   //   if (navigator.mediaDevices.getUserMedia) {
       navigator.mediaDevices.getUserMedia({  audio: true, video: true })
        .then(function (stream) {
          console.log('here I come');
            console.log(stream);
          gotStream(stream)
      })
        .catch(function (e) { e.name + ": " + e.message; }); 
   //   }
    } else {
      alert('Media Stream does not support by your browser');
    }

    // if (checkUserMediaSupport) {
    //     //set media constraints based on the button clicked. Audio only should be initiated by default
    //    streamConstraints === { video: { facingMode: 'user' }, audio: true };

    //         navigator.mediaDevices.getUserMedia(
    //         streamConstraints
    //         ).then(function (myStream) {
    //          //   document.getElementById("myVid").srcObject = myStream;

    //          gotStream(myStream)

    //         //    myPC.addStream(myStream);//add my stream to RTCPeerConnection

    //             //set var myMediaStream as the stream gotten. Will be used to remove stream later on
    //          //   myMediaStream = myStream;


    //         })
    //         .catch(function (e) { e.name + ": " + e.message; });  
    // }


    //   if (navigator.mediaDevices.getUserMedia) {
    // navigator.mediaDevices.getUserMedia({  audio: true, video: true })
    //     .then(function (stream) {
    //         console.log(stream);
    //       gotStream(stream)
    //   })
    //     .catch(function (e) { e.name + ": " + e.message; });      

    //   }
    
   

      function gotStream(stream) {

        console.log('hello stream function', getUser());
        console.log('hello', "localVideo-{{$nick}}");
      //  console.log('link', window.URL.createObjectURL(stream));
        document.getElementById("callButton").style.display = 'inline-block';
        document.getElementById("localVideo-{{$nick}}").srcObject = stream;
        document.getElementById("remoteVideo-{{$nick}}").srcObject = stream;

          //eval('pc_' + getUser()) = new PeerConnection(null);

          // const servers = {
          //   // iceServers: [
          //   //   //        {urls: 'stun:stun.l.google.com:19302'},
          //   //   //        {urls: stunServers},
          //   //   {
          //   //     urls: 'turn:52.88.28.2:3478',
          //   //     username: 'turnuser',
          //   //     credential: 'turn.pass'
          //   //   }
          //   // ]

          //   iceServers: [{
          //   //  urls: 'stun:stun.l.google.com:19302'

          // //  urls: 'turn:192.168.0.102:3478',
          // //        username: 'test',
          // //        credential: 'test'

          //       urls: 'turn:52.88.28.2:3478',
          //        username: 'turnuser',
          //        credential: 'turn.pass'

          //    }]
          // };

          console.log('server_conf', servers);


          // pc_user1 = new PeerConnection(servers);
          // pc_user2 = new PeerConnection(servers);
          // pc_user3 = new PeerConnection(servers);

           (async function() {

          // eval('pc_' + getUser()).addStream(await stream);
          // eval('pc_' + getUser()).onicecandidate = eval(await 'gotIceCandidate_' + getUser());
          // eval('pc_' + getUser()).onaddstream = eval(await 'gotRemoteStream_' + getUser());

          

    //   pc_user1 = pc_user1.close.bind(pc_user1);
    // pc_user2 = pc_user2.close.bind(pc_user2);
    // pc_user3 = pc_user3.close.bind(pc_user3);

          pc_user1 = new PeerConnection(servers);
          pc_user1.addStream(stream);
          pc_user1.onicecandidate = gotIceCandidate_user1;
          pc_user1.onaddstream = gotRemoteStream_user1;

          console.log('pc_user11', pc_user1);
          
          pc_user2 = new PeerConnection(servers);
          pc_user2.addStream(stream);
          pc_user2.onicecandidate = gotIceCandidate_user2;
          pc_user2.onaddstream = gotRemoteStream_user2;

          console.log('pc_user22', pc_user2);
          
          pc_user3 = new PeerConnection(servers);
          pc_user3.addStream(stream);
          pc_user3.onicecandidate = gotIceCandidate_user3;
          pc_user3.onaddstream = gotRemoteStream_user3;

          console.log('pc_user33', pc_user3);

        })()

      }

     

    });

   

    


      function createOffer() {

          console.log('create offer for', getUser());


          // eval('pc_' + getUser()).createOffer(
          //   eval('gotLocalDescription_' + getUser()),
          //   function(error) { console.log(error) },
          //   { 'mandatory': { 'OfferToReceiveAudio': true, 'OfferToReceiveVideo': true } }
          // );
          pc_user1.createOffer(
            gotLocalDescription_user1,
            function(error) { console.log(error) },
            { 'mandatory': { 'OfferToReceiveAudio': true, 'OfferToReceiveVideo': true } }
          );
           pc_user2.createOffer(
             gotLocalDescription_user2,
             function(error) { console.log(error) },
             { 'mandatory': { 'OfferToReceiveAudio': true, 'OfferToReceiveVideo': true } }
           );
            pc_user3.createOffer(
             gotLocalDescription_user3,
             function(error) { console.log(error) },
             { 'mandatory': { 'OfferToReceiveAudio': true, 'OfferToReceiveVideo': true } }
           );
        console.log("I CALLED!")
      }


        function createAnswer_user1() {
          console.log("createAnswer_user1");
           pc_user1.createAnswer(
             gotLocalDescription_user1,
             function(error) { console.log(error) },
             { 'mandatory': { 'OfferToReceiveAudio': true, 'OfferToReceiveVideo': true } }
           );
           console.log("USER ANSWERED!");
        }

        function gotLocalDescription_user1(description){
          console.log('localDesc_user1', description)
          pc_user1.setLocalDescription(description);
          sendMessage(description, getUser());
        }

        function gotIceCandidate_user1(event){
          if (event.candidate) {
            sendMessage({
              type: 'candidate',
              label: event.candidate.sdpMLineIndex,
              id: event.candidate.sdpMid,
              candidate: event.candidate.candidate
            }, getUser());
          }
        }

        function gotRemoteStream_user1(event){
          console.log('getRmoteStream formm user1', event.stream);

       //   document.getElementById("remoteVideo-user3").srcObject = stream;
          document.getElementById("remoteVideo-user1").srcObject = event.stream;
          console.log('after remote media set');
        }


      //  console.log('call create for: ', getUser());

        function getUser() {
          if("{{$nick}}" == "user1")
            return "user2";

          return "user1";
        }

        //  function getUser() {
        //   if("{{$nick}}" == "user3")
        //     return "user1";
        //   elseif("{{$nick}}" == 'user1')
        //     return "user2";
         
        //     return "user1";
          

        //   // if("{{$nick}}" == "user1")
        //   //   return "user2";

        //   // return "user1";
        // }



function createAnswer_user2() {
  console.log("createAnswer_user2");
   pc_user2.createAnswer(
     gotLocalDescription_user2,
     function(error) { console.log(error) },
     { 'mandatory': { 'OfferToReceiveAudio': true, 'OfferToReceiveVideo': true } }
   );
   console.log("WE ANSWERED!");
}

function gotLocalDescription_user2(description){
  console.log('localDESC_user2', description);
  pc_user2.setLocalDescription(description);
   console.log('pc_user2_user2', pc_user2);
  sendMessage(description, getUser());
}

function gotIceCandidate_user2(event){
  if (event.candidate) {
    sendMessage({
      type: 'candidate',
      label: event.candidate.sdpMLineIndex,
      id: event.candidate.sdpMid,
      candidate: event.candidate.candidate
    }, getUser());
  }
}

function gotRemoteStream_user2(event){
  console.log('remote user stream user2', event.stream)

  console.log('remote user video stream user2', event.stream.getVideoTracks())

  document.getElementById("remoteVideo-user2").srcObject = event.stream;

}


// For User 3
function createAnswer_user3() {
  console.log("createAnswer_user3");
   pc_user3.createAnswer(
     gotLocalDescription_user3,
     function(error) { console.log(error) },
     { 'mandatory': { 'OfferToReceiveAudio': true, 'OfferToReceiveVideo': true } }
   );
   console.log("WE ANSWERED!");
}

function gotLocalDescription_user3(description){
  console.log('localDESC_user3', description);
  pc_user3.setLocalDescription(description);
   console.log('pc_user3_user3', pc_user3);
  sendMessage(description, 'user3');
}

function gotIceCandidate_user3(event){
  if (event.candidate) {
    sendMessage({
      type: 'candidate',
      label: event.candidate.sdpMLineIndex,
      id: event.candidate.sdpMid,
      candidate: event.candidate.candidate
    }, 'user3');
  }
}

function gotRemoteStream_user3(event){
  console.log('remote user stream user3', event.stream)

  document.getElementById("remoteVideo-user3").srcObject = event.stream;

}

      //
      // function send(){
      //     var data = 'Data for sending: ' + Math.random();
      //     conn.send(data);
      //     console.log('Sent: ' + data);
      // }

      function showSnackBar(msg, displayTime) {
          document.getElementById('snackbar').innerHTML = msg;
          document.getElementById('snackbar').className = document.getElementById('snackbar').getAttribute('class') + " show";

          setTimeout(function () {
              $("#snackbar").html("").removeClass("show");
          }, displayTime);
      }


      conn.onopen = function(e){
        console.log("Connection established!", e);

        showSnackBar("Chat WebSocket Connected !", 3000);
      }



      conn.onmessage = function(str){
        //obj = Object.create(message[0]);
        //console.log("we got str: " + str.data);
     //   alert('into the messages')
        console.log('I am in message section');
        message = JSON.parse(str.data);

        console.log('I amkk', message);
          if(message.dst === "{{$nick}}")
          {
            var src = message.src;
            var variable = 'pc_' + src;

            console.log(message);
            if (message.type === 'offer') {
                console.log('message.type', message.type );
                console.log('offer which user by', variable );
                console.log('offer which user for', src );
                eval(variable).setRemoteDescription(new SessionDescription(message));
                //createAnswer_user1();

                console.log('remote desciption set', 'pc_'+ src);
                eval("createAnswer_" + src + '()');
            }
            else if (message.type === 'answer') {
              console.log('message.type', message.type );
                console.log('answer which user by', variable );

                console.log('answer session message from remote user', message);

                console.log('answer session object', new SessionDescription(message));

              eval(variable).setRemoteDescription(new SessionDescription(message));
            }
            else if (message.type === 'candidate') {
               console.log('message.type', message.type );
                console.log('candidate which user by', variable );
                console.log('candidate which user for', src );
                console.log('candidate remote desciption set', 'pc_'+ src);
                //alert(3);
                var candidate = new IceCandidate({sdpMLineIndex: message.label, candidate: message.candidate});
                eval(variable).addIceCandidate(candidate);
                //alert(4);
            }
          }


      }

      function sendMessage(message, dst){
        var str = JSON.stringify(message);
        str = str.replace("{", '{"src":"{{$nick}}", "dst":"' + dst + '",');
       console.log("We sending: ", str);
          console.log("conn: ", conn);
          conn.send(str);
      }


    conn.onclose= function(evt) { 
        console.log("DISCONNECTED");
    }

    conn.onerror = function(evt) { 
        console.log('<span style="color: red;">ERROR:</span> ' + evt.data);
    }


 

      // function sendMessage(message){
      //   var str = JSON.stringify(message);
      //  console.log("We sending: ", str);
      //     //console.log("we sending: ", str);
      //     conn.send(str);
      // }
      

    </script>
</html>
