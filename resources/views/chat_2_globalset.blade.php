<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>chat</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style type="text/css">
          html { height: 100%; }
          body { height: 100%; margin: 0; background: #333; text-align: center; }
          .video { height: 200px; margin-top: 5%; background: #000; }
          .localVideo { width: 150px; position: absolute; right: 1.1em; bottom: 1em; border: 1px solid #333; background: #000; }
          #callButton {
            position: absolute;
            /* display: none; */
            left: 50%;
            font-size: 2em;
            bottom: 60%;
            border-radius: 1em;
        }
          
        </style>
    </head>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>

    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
              <h1 style="color:white;">{{$nick}}</h1>
              <br>
              <video id="localVideo-{{$nick}}"  class="localVideo" autoplay muted></video>

              <button id="callButton" onclick="createOffer()">✆</button>
              <video id="remoteVideo-user1" class='video' autoplay></video>
              <video id="remoteVideo-user2" class='video' autoplay></video>
              <video id="remoteVideo-user3" class='video' autoplay></video>

            </div>
        </div>

        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        First PC, first offer: <span id="11"></span><br/>
        First PC, second offer: <span id="12"></span><br/>
        Second PC, first offer: <span id="21"></span><br/>
        Second PC, second offer: <span id="22"></span><br/> -->
    </body>
    <script>

      var PeerConnection = window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
      var IceCandidate = window.mozRTCIceCandidate || window.RTCIceCandidate;
      var SessionDescription = window.mozRTCSessionDescription || window.RTCSessionDescription;
      navigator.getUserMedia = navigator.getUserMedia || navigator.mozGetUserMedia || navigator.webkitGetUserMedia;

      // var pc_user1;
      // var pc_user2;
      // var pc_user3;

      var pc;

      console.log('mypc', pc);


      // Step 1. getUserMedia
    //   navigator.mediaDevices.getUserMedia(
    //       { audio: true, video: true },
    //  //   { video: true },
    //     gotStream,
    //     function(error) { console.log(error) }
    //   );


      if (navigator.mediaDevices.getUserMedia) {
        console.log('here');
    navigator.mediaDevices.getUserMedia({  audio: true, video: true })
        .then(function (stream) {
            console.log(stream);
            console.log('mystream', stream)
          gotStream(stream)
      })
        .catch(function (e) { e.name + ": " + e.message; });      

      }

      function gotStream(stream) {

        console.log('hello stream function', getUser());
        console.log('hello', "localVideo-{{$nick}}");
      //  console.log('link', window.URL.createObjectURL(stream));
        document.getElementById("callButton").style.display = 'inline-block';
        document.getElementById("localVideo-{{$nick}}").srcObject = stream;

          //eval('pc_' + getUser()) = new PeerConnection(null);

          const servers = {
            // iceServers: [
            //   //        {urls: 'stun:stun.l.google.com:19302'},
            //   //        {urls: stunServers},
            //   {
            //     urls: 'turn:52.88.28.2:3478',
            //     username: 'turnuser',
            //     credential: 'turn.pass'
            //   }
            // ]

            iceServers: [{
            //  urls: 'stun:stun.l.google.com:19302'

          //  urls: 'turn:192.168.0.102:3478',
          //        username: 'test',
          //        credential: 'test'

           //  urls: 'turn:192.168.0.119:3478',

                //  urls: 'turn:webrtc-test.ru',
                //  username: 'test',
                //  credential: 'test'

                urls: 'turn:52.88.28.2:3478',
                username: 'turnuser',
                credential: 'turn.pass'
             }]
          };

          console.log('server_conf', servers);


          // pc_user1 = new PeerConnection(servers);
          // pc_user2 = new PeerConnection(servers);

          
          // eval('pc_' + getUser()).addStream(stream);
          // eval('pc_' + getUser()).onicecandidate = eval('gotIceCandidate_' + getUser());
          // eval('pc_' + getUser()).onaddstream = eval('gotRemoteStream_' + getUser());

          pc = new PeerConnection(servers);
          pc.addStream(stream);
          pc.onicecandidate = gotIceCandidate_user;
          pc.onaddstream = gotRemoteStream_user;


          // pc_user1 = new PeerConnection(servers);
          // pc_user1.addStream(stream);
          // pc_user1.onicecandidate = gotIceCandidate_user1;
          // pc_user1.onaddstream = gotRemoteStream_user1;

          // console.log('pc_user11', pc_user1);
          
          // pc_user2 = new PeerConnection(servers);
          // pc_user2.addStream(stream);
          // pc_user2.onicecandidate = gotIceCandidate_user2;
          // pc_user2.onaddstream = gotRemoteStream_user2;

          // console.log('pc_user22', pc_user2);
          // //
          // pc_user3 = new PeerConnection(servers);
          // pc_user3.addStream(stream);
          // pc_user3.onicecandidate = gotIceCandidate_user3;
          // pc_user3.onaddstream = gotRemoteStream_user3;


      }

      function createOffer() {

        //  console.log('create offer for', getUser());


              pc.createOffer(
              gotLocalDescription_user,
                function(error) { console.log(error) },
                { 'mandatory': { 'OfferToReceiveAudio': true, 'OfferToReceiveVideo': true } }
             );


          // eval('pc_' + getUser()).createOffer(
          //   eval('gotLocalDescription_' + getUser()),
          //   function(error) { console.log(error) },
          //   { 'mandatory': { 'OfferToReceiveAudio': true, 'OfferToReceiveVideo': true } }
          // );

          // pc_user1.createOffer(
          //   gotLocalDescription_user1,
          //   function(error) { console.log(error) },
          //   { 'mandatory': { 'OfferToReceiveAudio': true, 'OfferToReceiveVideo': true } }
          // );
          //  pc_user2.createOffer(
          //    gotLocalDescription_user2,
          //    function(error) { console.log(error) },
          //    { 'mandatory': { 'OfferToReceiveAudio': true, 'OfferToReceiveVideo': true } }
          //  );
        console.log("WE CALLED!")
      }


        function createAnswer_user() {
          console.log("createAnswer_user");
           pc.createAnswer(
             gotLocalDescription_user,
             function(error) { console.log(error) },
             { 'mandatory': { 'OfferToReceiveAudio': true, 'OfferToReceiveVideo': true } }
           );
           console.log("WE ANSWERED!");
        }

        function gotLocalDescription_user(description){
          console.log('localDesc_user', description)
          pc.setLocalDescription(description);
          sendMessage(description);
        }

        function gotIceCandidate_user(event){
          if (event.candidate) {
            sendMessage({
              type: 'candidate',
              label: event.candidate.sdpMLineIndex,
              id: event.candidate.sdpMid,
              candidate: event.candidate.candidate
            });
          }
        }

        function gotRemoteStream_user(event){
          console.log('getRmoteStream form user', event.stream)

          document.getElementById("remoteVideo-user3").srcObject = event.stream;

        }




        function getUser() {
          if("{{$nick}}" == "user1")
            return "user2";

          return "user1";
        }


function createAnswer_user2() {
  console.log("createAnswer_user2");
   pc_user2.createAnswer(
     gotLocalDescription_user2,
     function(error) { console.log(error) },
     { 'mandatory': { 'OfferToReceiveAudio': true, 'OfferToReceiveVideo': true } }
   );
   console.log("WE ANSWERED!");
}

function gotLocalDescription_user2(description){
  console.log('localDESC_user2', description);
  pc_user2.setLocalDescription(description);
   console.log('pc_user2_user2', pc_user2);
  sendMessage(description, getUser());
}

function gotIceCandidate_user2(event){
  if (event.candidate) {
    sendMessage({
      type: 'candidate',
      label: event.candidate.sdpMLineIndex,
      id: event.candidate.sdpMid,
      candidate: event.candidate.candidate
    }, getUser());
  }
}

function gotRemoteStream_user2(event){
  console.log('remote user stream user2', event.stream)

  document.getElementById("remoteVideo-user2").srcObject = event.stream;

}


// For User 3
function createAnswer_user3() {
  console.log("createAnswer_user3");
   pc_user3.createAnswer(
     gotLocalDescription_user3,
     function(error) { console.log(error) },
     { 'mandatory': { 'OfferToReceiveAudio': true, 'OfferToReceiveVideo': true } }
   );
   console.log("WE ANSWERED!");
}

function gotLocalDescription_user3(description){
  console.log('localDESC_user3', description);
  pc_user3.setLocalDescription(description);
   console.log('pc_user3_user3', pc_user3);
  sendMessage(description, getUser());
}

function gotIceCandidate_user3(event){
  if (event.candidate) {
    sendMessage({
      type: 'candidate',
      label: event.candidate.sdpMLineIndex,
      id: event.candidate.sdpMid,
      candidate: event.candidate.candidate
    }, getUser());
  }
}

function gotRemoteStream_user3(event){
  console.log('remote user stream user3', event.stream)

  document.getElementById("remoteVideo-user3").srcObject = event.stream;

}





      // WebSocket
   //   var conn = new WebSocket('wss://webrtc.local:443/wss2/');
 //  var conn = new WebSocket("wss://webrtc-test.ru/wss2/");
  //  var conn = new WebSocket('wss:/scholars-home.org/chatsocket');

    var conn = new WebSocket('ws:/localhost:8080');
    console.log('websocekt', conn)
      conn.open = function(e){
        console.log("Connection established!", e);
      }

    // Let us open a web socket
              //  var conn = new WebSocket("ws://localhost:9998/echo");
				
              //  conn.onopen = function() {
                  
              //     // Web Socket is connected, send data using send()
              //     conn.send("Message to send");
              //     alert("Message is sent...");
              //  };

      conn.onmessage = function(str){
        //obj = Object.create(message[0]);
        //console.log("we got str: " + str.data);

        console.log('I am in message section');


        message = JSON.parse(str.data);

        console.log('I amkk', message);


          // if(message.dst === "{{$nick}}")
          // {
         //   var src = message.src;
            var variable = 'pc';

            console.log(message);
            if (message.type === 'offer') {
                console.log('message.type', message.type );
                eval(variable).setRemoteDescription(new SessionDescription(message));
                //createAnswer_user1();
                eval("createAnswer_user" + '()');
            }
            else if (message.type === 'answer') {

              eval(variable).setRemoteDescription(new SessionDescription(message));
            }
            else if (message.type === 'candidate') {
                //alert(3);
                var candidate = new IceCandidate({sdpMLineIndex: message.label, candidate: message.candidate});
                eval(variable).addIceCandidate(candidate);
                //alert(4);
            }
    //      }


      }
      //
      // function send(){
      //     var data = 'Data for sending: ' + Math.random();
      //     conn.send(data);
      //     console.log('Sent: ' + data);
      // }

      function sendMessage(message){
        var str = JSON.stringify(message);
        str = str.replace("{", '{"src":"{{$nick}}", "dst":"' + 'all users' + '",');
       console.log("We sending: ", str);
          console.log("conn: ", conn);
          conn.send(str);
      }

      // function sendMessage(message){
      //   var str = JSON.stringify(message);
      //  console.log("We sending: ", str);
      //     //console.log("we sending: ", str);
      //     conn.send(str);
      // }

    </script>
</html>
