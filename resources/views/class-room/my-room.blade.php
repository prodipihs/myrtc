@extends('layouts.users')

@section('header_script')
<script src="{{asset('js/multi/RTCMultiConnection.js?v4')}}"></script>
<script src="https://13.229.123.89:9001/socket.io/socket.io.js"></script>

<style>
button {
  margin: 0 3px 10px 0;
}

button:last-of-type {
  margin: 0;
}

p.borderBelow {
  margin: 0 0 20px 0;
  padding: 0 0 20px 0;
}

video {
  vertical-align: top;
  --width: 25vw;
  width: var(--width);
  height: calc(var(--width) * 0.5625);
}

video:last-of-type {
  margin: 0 0 20px 0;
}

video#gumVideo {
  margin: 0 20px 20px 0;
}
</style>
@endsection

@section('content')
<div class="row-fluid" style="margin-top: 10px;">

<h1 style="text-align:center;">Welcome to The Room</h1>
<div id="container" style="text-align: center;">
    
    <div>
        <button id="start">Start camera</button>
        <button id="record" disabled>Start Recording</button>
        <button id="play" disabled>Play</button>
        <button id="download" disabled>Download</button>
        <p style="display:none;">Echo cancellation: <input type="checkbox" id="echoCancellation"></p>
    </div>


    <video id="gum" playsinline autoplay muted></video>
    <video id="recorded" playsinline loop></video>
    <div>
        <span id="errorMsg"></span>
    </div>

</div>


</div>

<script src="{{asset('js/multi/record.js?v2')}}"></script>

@endsection
