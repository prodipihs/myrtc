@extends('layouts.users')

@section('header_script')
<script src='https://meet.jit.si/external_api.js'></script>

<style>
#container{
    margin: 0 15px 100px 15px;
}
button {
  margin: 0 3px 10px 0;
}

button:last-of-type {
  margin: 0;
}

p.borderBelow {
  margin: 0 0 20px 0;
  padding: 0 0 20px 0;
}

video {
  vertical-align: top;
  --width: 25vw;
  width: var(--width);
  height: calc(var(--width) * 0.5625);
}

video:last-of-type {
  margin: 0 0 20px 0;
}

video#gumVideo {
  margin: 0 20px 20px 0;
}
</style>
@endsection

@section('content')
<div class="row-fluid" style="margin-top: 10px;">

<h3 style="text-align:center;">Welcome to Scholars Room</h3>
<div id="container" style="text-align: center;">
    

</div>


</div>

<script>
    const domain = '18.138.254.59';
    const options = {
        roomName: 'TestRoom1',
        height: 500,
        parentNode: document.querySelector('#container'),
        userInfo: {
        email: 'prodip50801@gmail.com'
        },

    };
    const api = new JitsiMeetExternalAPI(domain, options);
    api.executeCommand('displayName', 'Prodip Chan');
    api.executeCommand('subject', 'Chemistry');
    api.executeCommand('avatarUrl', 'https://scholars-home.org/img/home/logo.png');

</script>

@endsection
