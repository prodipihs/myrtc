<!doctype html>
<html class="no-js h-100" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Scholars Home</title>
    <meta name="description" content="A high-quality &amp; free Bootstrap admin dashboard template pack that comes with lots of templates and components.">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" id="main-stylesheet" data-version="1.1.0" href="https://scholars-home.org/css/shards-dashboards.1.1.0.min.css">
   
    <link rel="stylesheet" href="https://scholars-home.org/css/sidenav.css?ii">
    <link rel="stylesheet" href="https://scholars-home.org/css/search.css?v3">
    <link rel="stylesheet" href="https://scholars-home.org/css/extras.1.1.0.min.css">

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>


    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js">


        <script async defer src="https://buttons.github.io/buttons.js"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
 

    <style>
        @media(max-width: 767px){
            .m-search{width:83% !important;}
            .d-md-inline-block{color: #888585;}
            .text-nowrap{ margin-left: 1em;}
            .m-hide{display:none !important;}
        }
        .select2-container {
            display: inherit !important;
        }
        a { text-decoration: none !important;}
    </style>

    @yield('header_script')
</head>

<body class="h-100">

<div class="modal fade" id="search" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Search Teacher</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div style="padding-bottom:10px">
                    <b>Subject</b>
                </div>
                <div class="form-group">
                    <select class="form-control select-init select-bar" name="topics[]" id="topics" multiple="true">
                    </select>
                </div>
                <div style="padding-bottom:10px">
                    <b>Country</b>
                </div>
                <div class="form-group">
                    <select class="form-control select-init" name="country[]" id="country" multiple="true">
                    </select>
                </div>
                <div style="padding-bottom:10px">
                    <b>Language</b>
                </div>
                <div class="form-group">
                    <select class="form-control select-init" name="language[]" id="language"  multiple="true">
                    </select>
                </div>
                <div class="form-group">
                    <button class="btn btn-search btn-primary">
                        Search
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
               


<div class="color-switcher animated">
    <h5>Accent Color</h5>
    <ul class="accent-colors">
        <li class="accent-primary active" data-color="primary">
            <i class="material-icons">check</i>
        </li>
        <li class="accent-secondary" data-color="secondary">
            <i class="material-icons">check</i>
        </li>
        <li class="accent-success" data-color="success">
            <i class="material-icons">check</i>
        </li>
        <li class="accent-info" data-color="info">
            <i class="material-icons">check</i>
        </li>
        <li class="accent-warning" data-color="warning">
            <i class="material-icons">check</i>
        </li>
        <li class="accent-danger" data-color="danger">
            <i class="material-icons">check</i>
        </li>
    </ul>
    <div class="close">
        <i class="material-icons">close</i>
    </div>
</div>
{{--<div class="color-switcher-toggle animated pulse infinite">--}}
{{--<i class="material-icons">settings</i>--}}
{{--</div>--}}

<div style ="position: fixed;
    bottom: 80px;
    right: -49px;
    z-index: 100;
    -webkit-transform: rotate(-90deg);">
    <a class="btn btn-primary card-small" href="" style="box-shadow: -4px 5px 2px #cccccc;font-size: 20px;font-weight: bold;" title="user guide for your help" target="_blank">
        <i class="fa fa-address-book"></i> <b>User Guide</b>
    </a>
</div>


<div class="container-fluid">
    <div class="row">
        <aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0" id="sidebar-main">
            <div class="main-navbar">
                <nav class="navbar align-items-stretch navbar-light bg-white flex-md-nowrap border-bottom p-0">
                    <a class="navbar-brand w-100 mr-0" style="line-height: 25px;" href="{{url('/')}}">
                        <div class="d-table m-auto">
                            
                            <br/>
                          
                                <span class="d-none d-md-inline ml-1">Dashboard (Teacher)</span>
                        
                        </div>
                    </a>
                    <a class="toggle-sidebar d-sm-inline d-md-none d-lg-none">
                        <i class="material-icons">&#xE5C4;</i>
                    </a>
                </nav>
            </div>
            <form action="#" class="main-sidebar__search w-100 border-right d-sm-flex d-md-none d-lg-none">
                <div class="input-group input-group-seamless">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <i class="fas fa-search"></i>
                        </div>
                    </div>
                    <input class="navbar-search form-control" type="text" placeholder="Search teacher by topic, language or country..." aria-label="Search"> </div>
            </form>
            <div class="nav-wrapper">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link {{ Request::segment(1) === 'home' ? 'active' : null }}" href="">
                            <i class="material-icons">edit</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                  
                        <li class="nav-item">
                            <a class="nav-link {{ Request::segment(1) === 'resource' ? 'active' : null }}" href="{{url('/')}}">
                                <i class="material-icons">vertical_split</i>
                                <span>Resource</span>
                            </a>
                        </li>
                
                    <li class="nav-item">
                        <a class="nav-link {{ Request::segment(1) === 'profile' ? 'active' : null }}" href="{{url('/')}}">
                            <i class="material-icons">&#xE7FD;</i>
                            <span>Your Profile</span>
                        </a>
                    </li>
                 
                    <li class="nav-item">
                        <a class="nav-link {{ Request::segment(1) === 'appointments' ? 'active' : null }}" href="{{url('/')}}">
                            <i class="material-icons">vertical_split</i>
                            <span>Appointments</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Request::segment(1) === 'invoices' ? 'active' : null }}" href="{{url('/')}}">
                            <i class="material-icons">vertical_split</i>
                            <span>Invoices</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Request::segment(1) === 'messages' ? 'active' : null }}" href="{{url('/')}}">
                            <i class="far fa-envelope"></i> <span>Messages</span>
                        </a>
                    </li>
                </ul>
            </div>
        </aside>

        <main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3" style="position: fixed;">
            <div class="main-navbar sticky-top bg-white">
                <!-- Main Navbar -->
                <nav class="navbar align-items-stretch navbar-light flex-md-nowrap p-0">
                    <form action="#" class="main-navbar__search w-100 d-md-flex d-lg-flex m-search">
                        <div class="input-group input-group-seamless">
                            <div class="input-group-prepend">
                                <div class="input-group-text d-none">
                                    <i class="fas fa-search fa-2x"></i>
                                </div>
                            </div>
                            <input class="navbar-search form-control" type="text" placeholder="Search teacher by topic, language or country" aria-label="Search"> </div>
                    </form>
                    <ul class="navbar-nav border-left flex-row m-hide">
                        <!-- <li class="nav-item border-right dropdown notifications">
                          <a class="nav-link nav-link-icon text-center" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="nav-link-icon__wrapper">
                              <i class="material-icons">&#xE7F4;</i>
                              <span class="badge badge-pill badge-danger">2</span>
                            </div>
                          </a>
                          <div class="dropdown-menu dropdown-menu-small" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" href="#">
                              <div class="notification__icon-wrapper">
                                <div class="notification__icon">
                                  <i class="material-icons">&#xE6E1;</i>
                                </div>
                              </div>
                              <div class="notification__content">
                                <span class="notification__category">Analytics</span>
                                <p>Your website’s active users count increased by
                                  <span class="text-success text-semibold">28%</span> in the last week. Great job!</p>
                              </div>
                            </a>
                            <a class="dropdown-item" href="#">
                              <div class="notification__icon-wrapper">
                                <div class="notification__icon">
                                  <i class="material-icons">&#xE8D1;</i>
                                </div>
                              </div>
                              <div class="notification__content">
                                <span class="notification__category">Sales</span>
                                <p>Last week your store’s sales count decreased by
                                  <span class="text-danger text-semibold">5.52%</span>. It could have been worse!</p>
                              </div>
                            </a>
                            <a class="dropdown-item notification__all text-center" href="#"> View all Notifications </a>
                          </div>
                        </li> -->
                        <li class="nav-item dropdown">
  
                            <a class="nav-link dropdown-toggle text-nowrap px-3" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                <img class="user-avatar rounded-circle mr-2" src="" alt="User Avatar" style="width:40px;height:40px;">
                                <span class="d-none d-md-inline-block" id="userName">username</span>
                                <span class="d-none" id="userAccID">UserID</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-small">
                                <a class="dropdown-item"  href="{{url('/')}}">
                                    <i class="material-icons">&#xE7FD;</i> Profile</a>
                                <a class="dropdown-item" href="{{url('/')}}">
                                    <i class="material-icons">security</i> Set Security Question</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item text-danger" href="{{url('/')}}">
                                    <i class="material-icons text-danger">&#xE879;</i> Logout </a>
                            </div>
                        </li>
                    </ul>
                    <nav class="nav">
                        <a onclick="openNav()" href="#" class="nav-link nav-link-icon toggle-sidebar d-md-inline d-lg-none text-center border-left" data-toggle="collapse" data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
                            <i class="material-icons">&#xE5D2;</i>
                        </a>

                        <div id="mySidenav" class="sidenav">
                           
                            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                            <hr>

                            <a class="nav-link text-nowrap px-3" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                <img class="user-avatar rounded-circle mr-2" src="" alt="User Avatar" style="width:40px;height:40px;">
                                <span class="d-md-inline-block">user_title</span>
                            </a>

                            <a class="dropdown-item"  href="{{url('/')}}">
                                <i class="material-icons">&#xE7FD;</i> Profile</a>
                            <a class="dropdown-item" href="{{url('/')}}">
                                <i class="material-icons">security</i> Set Security Question</a>




                            <a href="{{url('/')}}" class="{{ Request::segment(1) === 'home' ? 'btn-primary' : "" }}" style="color:{{ Request::segment(1) === 'home' ? 'white' : "" }};">Dashboard</a>
                          
                                <a href="{{url('/')}}" class="{{ Request::segment(1) === 'resource' ? 'btn-primary' : "" }}" style="color:{{ Request::segment(1) === 'resource' ? 'white' : "" }};">Resource</a>
   

                       

                                <a href="{{url('/')}}"  class="{{ Request::segment(1) === 'schedule-edit' ? 'btn-primary' : "" }}" style="color:{{ Request::segment(1) === 'schedule-edit' ? 'white' : "" }};">Your Schedule</a>
                          
                            <a href="{{url('/')}}"  class="{{ Request::segment(1) === 'appointments' ? 'btn-primary' : "" }}" style="color:{{ Request::segment(1) === 'appointments' ? 'white' : "" }};">Appointments</a>
                            <a href="{{url('/')}}"  class="{{ Request::segment(1) === 'invoices' ? 'btn-primary' : "" }}" style="color:{{ Request::segment(1) === 'invoices' ? 'white' : "" }};">Invoices</a>
                            <a href="{{url('/')}}"  class="{{ Request::segment(1) === 'messages' ? 'btn-primary' : "" }}" style="color:{{ Request::segment(1) === 'messages' ? 'white' : "" }};">Messages</a>
                            <a class="dropdown-item text-danger" href="{{url('/')}}">
                                <i class="material-icons text-danger">&#xE879;</i> Logout </a>
                        </div>
                    </nav>
                </nav>
            </div>
            <!-- / .main-navbar -->
      
            <div class="header-js-message"></div>

            @yield('content')

            {{--<footer class="main-footer d-flex p-2 px-3 bg-white border-top">--}}
            {{--<ul class="nav">--}}
            {{--<li class="nav-item">--}}
            {{--<a class="nav-link" href="#">Home</a>--}}
            {{--</li>--}}
            {{--<li class="nav-item">--}}
            {{--<a class="nav-link" href="#">Services</a>--}}
            {{--</li>--}}
            {{--<li class="nav-item">--}}
            {{--<a class="nav-link" href="#">About</a>--}}
            {{--</li>--}}
            {{--<li class="nav-item">--}}
            {{--<a class="nav-link" href="#">Products</a>--}}
            {{--</li>--}}
            {{--<li class="nav-item">--}}
            {{--<a class="nav-link" href="#">Blog</a>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{--<span class="copyright ml-auto my-auto mr-2">Copyright © 2019 <a target="_blank" href="http://www.ihealthscreen.org">iHealthScreen Inc</a>. All rights reserved.--}}
            {{--</span>--}}
            {{--</footer>--}}
        </main>
    </div>
</div>
{{-- <div class="promo-popup animated">
    <a href="http://bit.ly/shards-dashboard-pro" class="pp-cta extra-action">
        <img src="https://dgc2qnsehk7ta.cloudfront.net/uploads/sd-blog-promo-2.jpg"> </a>
    <div class="pp-intro-bar"> Need More Templates?
        <span class="close">
          <i class="material-icons">close</i>
        </span>
        <span class="up">
          <i class="material-icons">keyboard_arrow_up</i>
        </span>
    </div>
    <div class="pp-inner-content">
        <h2>Shards Dashboard Pro</h2>
        <p>A premium & modern Bootstrap 4 admin dashboard template pack.</p>
        <a class="pp-cta extra-action" href="http://bit.ly/shards-dashboard-pro">Download</a>
    </div>
</div> --}}

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
<script src="https://unpkg.com/shards-ui@latest/dist/js/shards.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Sharrre/2.0.1/jquery.sharrre.min.js"></script>

@yield('footer_script')
</body>
</html>
