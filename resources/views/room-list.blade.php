<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Create Room</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" crossorigin="anonymous">

    <style>
    a:hover {
    text-decoration: none;
    }
    .list{
        background-color: #efefef;
        display: inherit;
        padding: 6px;
    }
    a.list:hover {
    background-color: #b9b5b5;
    }
    .jumbotron {
    padding-top: 1px;
    padding-bottom: 20px;
    }
    .btn-save{ padding: 10px 25px;}
    .mgs-success{color: green;font-weight: bold;}
    </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">


            <div class="jumbotron text-center">
            <h1>WebRTC</h1>
            <!-- Trigger the modal with a button -->
            <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#myModal">Create New Room</button>
             <button type="button" class="btn btn-success btn-md" data-toggle="modal" data-target="#myAddUserModal" style="padding: 6px 40px;">Add User</button>
            <!-- <a><a href="" class="btn btn-primary">Create New Room</a></p> -->
            </div>

            <div class="container">
            <div class="row">

            <div class="col-sm-4" style="transform: translate(100%);text-align: center;">
            <h3>Room List </h3>
            <p><a href="enter-room/1001" class="list" data-toggle="modal" data-target="#myAddUserModal">Tone Up <span>(1)</span></a></p>
            <p><a href="enter-room/1002" class="list" data-toggle="modal" data-target="#myAddUserModal">ADDA <span>(3)</span></a></p>
            <p><a href="enter-room/1003" class="list" data-toggle="modal" data-target="#myAddUserModal">Designer <span>(0)</span></a></p>
            <p><a href="enter-room/1004" class="list" data-toggle="modal" data-target="#myAddUserModal">Developer <span>(0)</span></a></p>
            </div>

                <!-- <div class="col-sm-4" style="transform: translate(100%);text-align: center;">
                @if(session()->has('success'))
                <div class="mgs-success"> {{ session()->get('success') }}</div>
                @endif
                <h3>Room List </h3>
                @if(!empty($rooms))
                @foreach($rooms as $room)
                <p><a href="enter-room/{{ $room->id }}" class="list" data-toggle="modal" data-target="#myAddUserModal">{{ $room->name }} <span>({{ $room->count_users }})</span></a></p>
                @endforeach
                @else
                <p>No room created</p>
                @endif
                </div> -->
                
            </div>
            </div>


            <!-- Create Room Modal -->
            <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Create Room</h4>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ secure_url('/create-room') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">Room Name:</label>
                        <input type="text" name="name" class="form-control" id="name">
                    </div>
 
                    <button type="submit" class="btn btn-default btn-save">Save</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </div>
            </div>
            </div>

            <!-- Add User Modal -->
            <div id="myAddUserModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add User</h4>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ secure_url('/add-user-to-room') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="username">User Name:</label>
                        <input type="text" name="username" class="form-control" id="username">
                    </div>

                    <div class="form-group">
                        <label for="password">Password:</label>
                        <input type="password" name="password" class="form-control" id="password">
                    </div>

                    <div class="form-group">
                    <label for="sel1">Select Room</label>
                    <select class="form-control" id="sel1" name="room_id">
                    <option value="1001">Tone Up</option>
                    <option value="1002">ADDA</option>
                    <option value="1003">Designer</option>
                    <option value="1004">Developer</option>
                    </select>
                    </div>

                    <!-- <div class="form-group">
                    <label for="sel1">Select Room</label>
                    <select class="form-control" id="sel1" name="room_id">
                    @if(!empty($rooms))
                    @foreach($rooms as $room)
                    <option value="{{ $room->id }}">{{ $room->name }}</option>
                    @endforeach
                    @else
                    <option value="">No room created</option>
                    @endif
                    </select>
                    </div> -->
 
                    <button type="submit" class="btn btn-default btn-save">Add</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </div>
            </div>
            </div>




            <!-- <div class="content">

                <div class="title m-b-md">
                    Rooms List
                </div> 

                <a href="" class="btn btn-primary">Create New Room</a>

            </div> -->
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script>
    </body>
</html>
