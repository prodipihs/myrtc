<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <!-- <a href="{{ url('register') }}">Register</a> -->
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Scholars Home
                </div>

                <div class="row" style="margin-top: 7px;">
        <a href="{{ url('/scholars-group-call') }}" style="margin-left: 20px;font-size: 2.5em;background-color: #53bbc5;padding: 10px;color: #fff;font-weight: bold;text-decoration: none;border-radius: 5px;" class="btn btn-success btn-sm">Scholars Group Call</a>   
    </div> 

    <div class="row" style="margin-top: 40px;">
      <a href="{{ url('/my-room') }}" style="margin-left: 20px;font-size: 1.5em;background-color: #53bbc5;padding: 10px;color: #fff;font-weight: bold;text-decoration: none;border-radius: 5px;" class="btn btn-success btn-sm">My Room</a>
    </div>

    <div class="row" style="margin-top: 40px;">
      <a href="{{ url('/scholars-room') }}" style="margin-left: 20px;font-size: 2em;background-color: #1d373a;padding: 10px;color: #fff;font-weight: bold;text-decoration: none;border-radius: 5px;" class="btn btn-success btn-sm">Scholars Room</a>

    </div>

            </div>
        </div>
    </body>
</html>
