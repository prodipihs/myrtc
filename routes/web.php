<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('group-call',array('as' => 'group-call','uses' => 'ClassRoomController@multiconnection_group_call'));
Route::get('scholars-group-call',array('as' => 'scholars-group-call','uses' => 'ClassRoomController@scholars_multiconnection_group_call'));

Route::get('/group-conference',array('as' => 'group-conference','uses' => 'ClassRoomController@group_conference'));

Route::get('scholars-room', array('as' => 'scholars-room', 'uses' => 'ClassRoomController@scholars_room'));

Route::get('my-room', array('as' => 'my-room', 'uses' => 'ClassRoomController@my_room'));

// Route 2
Route::get('/classroom', 'ChatController@chat_room_list');

Route::post('/create-room', 'ChatController@create_room');

Route::post('/add-user-to-room', 'ChatController@add_user_to_room');

Route::get('/enter-room/{id}', 'ChatController@enter_room_by_id');

Route::get('/home', 'ChatController@login')->name('home');

Route::get('/login', 'ChatController@login')->name('login');

Route::post('/chat', 'ChatController@chat')->name('chat');

Route::get('/test', 'ChatController@test')->name('test');

Route::resource('/rooms', 'RoomController');

Route::get('/setting_room/{id}', 'RoomController@setting_room')->name('setting_room');
Route::post('/setting_generate', 'RoomController@setting_generate')->name('setting_generate');








